﻿using KolejkaRezerwacji.Model;
using KolejkaRezerwacji.View;
using KolejkaRezerwacji.ViewModel.Extensions;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace KolejkaRezerwacji
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            PobierzPozycjeZamowienia(WaproExtension.Instance.IdObiektu);
            PobierzZamowieniaPrzyszle(WaproExtension.Instance.IdObiektu);
        }
        public PozycjaZamowieniaModel PozycjaZamowienia { get; set; }
        private void PobierzPozycjeZamowienia(decimal idObiektu)
        {
            PozycjaZamowienia = new PozycjaZamowieniaModel();
            using (SqlConnection sqlCnn = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PobierzPozycjeZamowienia(" + idObiektu + ")", sqlCnn);
                using (SqlDataReader read = sqlCmd.ExecuteReader())
                {
                    while (read.Read())
                    {
                        PozycjaZamowienia.NazwaArtykulu = read["NazwaArtykulu"].ToString();
                        PozycjaZamowienia.Zamowiono = Convert.ToDecimal(read["Zamowiono"].ToString());
                        PozycjaZamowienia.Jednostka = read["Jednostka"].ToString();
                        PozycjaZamowienia.Przelicznik = Convert.ToDecimal(read["Przelicznik"].ToString());
                        PozycjaZamowienia.NazwaKoloru = read["NazwaKoloru"].ToString();
                        PozycjaZamowienia.NumerKoloru = read["NumerKoloru"].ToString();
                        PozycjaZamowienia.Indeks = read["Indeks"].ToString();
                    }
                }
            }
        }
        private List<ZamowieniePrzyszleModel> _zamowieniePrzyszle;
        private List<ZamowieniePrzyszleModel> _zaznaczoneZamowienie;
        public List<ZamowieniePrzyszleModel> ZamowieniePrzyszle { get { return _zamowieniePrzyszle; } set { _zamowieniePrzyszle = value; } }
        public List<ZamowieniePrzyszleModel> ZaznaczoneZamowienie { get { return _zaznaczoneZamowienie; } set { _zaznaczoneZamowienie = value; } }
        private void PobierzZamowieniaPrzyszle(object idObiektu)
        {
            ZamowieniePrzyszle = new List<ZamowieniePrzyszleModel>();
            using (SqlConnection sqlConnection = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
            {
                SqlCommand sqlCommand = new SqlCommand("select * from imag.kl_func_PokazZamowieniaPrzyszlePozycja(" + idObiektu + ")", sqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataTable dt = new DataTable("Pozycje");
                da.Fill(dt);
                foreach (DataRowView item in dt.DefaultView)
                {
                    ZamowieniePrzyszleModel poz = new ZamowieniePrzyszleModel()
                    {
                        IdPozZamowienia = Convert.ToDecimal(item["IdPozZamowienia"].ToString()),
                        Sezon = item["Sezon"].ToString(),
                        IdFabryki = Convert.ToDecimal(item["IdFabryki"].ToString()),
                        Fabryka = item["Fabryka"].ToString(),
                        DataRealizacji = Convert.ToDateTime(item["DataRealizacji"].ToString()),
                        //DataRealizacjiSys = Convert.ToInt32(item["DataRealizacjiSys"]),
                        Zamowiono = Convert.ToDecimal(item["Zamowiono"].ToString()),
                        Przelicznik = Convert.ToDecimal(item["Przelicznik"].ToString()),
                        Jednostka = item["Jednostka"].ToString(),
                        ZamowionoSztuk = Convert.ToDecimal(item["Zamowiono"].ToString()),
                        CzyZarezerwowano = Convert.ToBoolean(item["CzyZarezerwowano"])
                    };
                    ZamowieniePrzyszle.Add(poz);
                }
            }
        }
        public static async Task ShowMessageBox(string message, string dialogHostName = "RootDialog")
        {
            var sampleMessageDialog = new MessageBoxView
            {
                Message =
                {
                    Text = message,
                    MinWidth = 150
                }
            };
            await DialogHost.Show(sampleMessageDialog, dialogHostName);
        }
    }
}