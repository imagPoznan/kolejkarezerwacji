﻿using KolejkaRezerwacji.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KolejkaRezerwacji
{
    public class WaproExtension
    {
        public static WaproExtension Instance { get; set; } = new WaproExtension();
        private bool isError;
        private bool isChecked;
        private decimal _WaproDbVersion = 8799;
        private decimal _idFirmy;
        private decimal _idMagazynu;
        private decimal _idObiektu;
        private decimal _idUzytkownika;
        public string SqlServer
        {
            get => WaproModel.Instance.SqlServer;
            set => WaproModel.Instance.SqlServer = value;
        }
        public string SqlDatabase
        {
            get => WaproModel.Instance.SqlDatabase;
            set => WaproModel.Instance.SqlDatabase = value;
        }
        public string SqlUser
        {
            get => WaproModel.Instance.SqlUser;
            set => WaproModel.Instance.SqlUser = value;
        }
        public string SqlPassword
        {
            get => WaproModel.Instance.SqlPassword;
            set => WaproModel.Instance.SqlPassword = value;
        }
        public decimal IdFirmy
        {
            get => WaproModel.Instance.IdFirmy;
            set => WaproModel.Instance.IdFirmy = value;
        }
        public decimal IdMagazynu
        {
            get => WaproModel.Instance.IdMagazynu;
            set => WaproModel.Instance.IdMagazynu = value;
        }
        public decimal IdObiektu
        {
            get => WaproModel.Instance.IdObiektu;
            set => WaproModel.Instance.IdObiektu = value;
        }
        public decimal IdUzytkownika
        {
            get => WaproModel.Instance.IdUzytkownika;
            set => WaproModel.Instance.IdUzytkownika = value;
        }
        public int ZakresOd
        {
            get => WaproModel.Instance.ZakresOd;
            set => WaproModel.Instance.ZakresOd = value;
        }
        public int ZakresDo
        {
            get => WaproModel.Instance.ZakresDo;
            set => WaproModel.Instance.ZakresDo = value;
        }
        public string OpisZakresu
        {
            get => WaproModel.Instance.OpisZakresu;
            set => WaproModel.Instance.OpisZakresu = value;
        }
        public int KodKontekstu
        {
            get => WaproModel.Instance.KodKontekstu;
            set => WaproModel.Instance.KodKontekstu = value;
        }
        public decimal IdObiektuNadrzednego
        {
            get => WaproModel.Instance.IdObiektuNadrzednego;
            set => WaproModel.Instance.IdObiektuNadrzednego = value;
        }
        public int Zarezerwowane;
        public string GetWaproConnectionString()
        {
            try
            {
                string[] argumetns = Environment.GetCommandLineArgs();
                string[] parametr = argumetns[1].Split(',');
                SqlServer = parametr[0].ToString();
                SqlDatabase = parametr[1].ToString();
                SqlUser = parametr[2].ToString();
                SqlPassword = parametr[3].ToString();
                string conString = "Data source=" + SqlServer + ";Initial Catalog=" + SqlDatabase + ";User Id=" + SqlUser + ";Password=" + SqlPassword + ";";
                return conString;
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Wystąpił problem z połączeniem się do bazy danych programu WAPRO Mag. Błąd: " + e.Message, "Błąd połączenia", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }
        public bool CheckWaproConnection()
        {
            try
            {
                if (isError)
                    return false;
                if (isChecked)
                    return true;
                decimal result = 0;
                using (SqlConnection sqlCon = new SqlConnection(GetWaproConnectionString()))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlCmd = new SqlCommand("select 1", sqlCon))
                    {
                        _ = decimal.TryParse(sqlCmd.ExecuteScalar().ToString(), out result);
                    }
                }
                if (result == 1)
                {
                    isChecked = true;
                    isError = false;
                    return isChecked;
                }
                else
                {
                    isChecked = false;
                    isError = true;
                    return isChecked;
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Wystąpił problem z połączeniem się do bazy danych programu WAPRO Mag. Błąd: " + e.Message, "Błąd połączenia", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }
        public void GetWaproParameters()
        {
            string[] arguments = Environment.GetCommandLineArgs();
            string[] parametr = arguments[2].Split(',');
            if (decimal.TryParse(parametr[0], out _idFirmy) == false)
            {
                _ = MessageBox.Show("Sprawdź czy zaznaczono opcję przekazywania parametru id_firmy w opcjach operacji dodatkowej.");
            }
            else
            {
                IdFirmy = _idFirmy;
            };
            if (decimal.TryParse(parametr[1], out _idMagazynu) == false)
            {
                _ = MessageBox.Show("Sprawdź czy zaznaczono opcję przekazywania parametru id_magazynu w opcjach operacji dodatkowej.");
            }
            else
            {
                IdMagazynu = _idMagazynu;
            };
            if (decimal.TryParse(parametr[2], out _idObiektu) == false)
            {
                _ = MessageBox.Show("Sprawdź czy zaznaczono opcję przekazywania parametru id_obiektu w opcjach operacji dodatkowej.");
            }
            else
            {
                IdObiektu = _idObiektu;
            };
            if (decimal.TryParse(parametr[3], out _idUzytkownika) == false)
            {
                _ = MessageBox.Show("Sprawdź czy zaznaczono opcję przekazywania parametru id_uzytkownika w opcjach operacji dodatkowej.");
            }
            else
            {
                IdUzytkownika = _idUzytkownika;
            };
        }
        public string GetWaproVersion()
        {
            string WaproDbVersion;
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetWaproConnectionString()))
                {
                    string Cmd = "select PRGWER from "+ SqlDatabase + ".dbo.WAPRODBSTATE where PRGKOD=3";
                    sqlCnn.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(Cmd,sqlCnn))
                    {
                        WaproDbVersion = sqlCmd.ExecuteScalar().ToString();
                    }
                }
                return WaproDbVersion;
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Wystąpił problem ze sprawdzeniem wersji programu WAPRO Mag. " + e.Message);
                throw;
            }
        }
        public void CheckWaproVersion()
        {
            if (Convert.ToDecimal(GetWaproVersion().Replace(".",""))> _WaproDbVersion)
            {
                _ = MessageBox.Show("Wykryto nieprawidłową wersję programu WAPRO Mag (" + Instance.GetWaproVersion() + "). Skontaktuj się z producentem aplikacji. Aplikacja zostanie zamknięta.", "Nieprawidłowa wersja", MessageBoxButton.OK, MessageBoxImage.Stop);
                Environment.Exit(3);
            }
        }
        internal void Odrezerwuj(decimal idPozZamowienia, decimal idObiektu)
        {
            try
            {
                string Command = "exec imag.kl_UsunZKolejkiRezerwacji " + idPozZamowienia.ToString() + ", " + idObiektu.ToString();
                using (SqlConnection sqlCon = new SqlConnection(GetWaproConnectionString()))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(Command, sqlCon))
                    {
                        _ = sqlCmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Wystąpił problem z usunięciem pozycji z kolejki rezerwacji. " + e.Message);
                throw;
            }
        }
        internal void Zarezerwuj(decimal idPozZamowienia, decimal idObiektu)
        {
            try
            {
                string Command = "exec imag.kl_DodajDoKolejkiRezerwacji " + idPozZamowienia.ToString() + ", " + idObiektu.ToString();
                using (SqlConnection sqlCon = new SqlConnection(GetWaproConnectionString()))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(Command, sqlCon))
                    {
                        _ = sqlCmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Wystąpił problem z rezerwacją pozycji. " + e.Message);
                throw;
            }
        }
    }
}
