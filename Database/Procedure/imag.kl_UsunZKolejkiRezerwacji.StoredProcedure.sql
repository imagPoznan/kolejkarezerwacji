/****** Object:  StoredProcedure [imag].[kl_UsunZKolejkiRezerwacji]    Script Date: 13.07.2021 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[kl_UsunZKolejkiRezerwacji]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imag].[kl_UsunZKolejkiRezerwacji] AS' 
END
GO


/*
-- =============================================
-- Author:		Karol Łyduch
-- Create date: 2021-07-08
-- Description:	Procedura dodaje pozycje do kolejki rezerwacji
-- =============================================
*/
ALTER	PROCEDURE [imag].[kl_UsunZKolejkiRezerwacji]
		(@IdPozycjiBZD numeric
		,@IdPozycjiZO numeric
		--,@DoRezerwacji decimal(16,6)
		)
AS
BEGIN
	if exists (select 1 from imag.KolejkaRezerwacji where IdPozycjiZO=@IdPozycjiZO and IdPozycjiBZD=@IdPozycjiBZD)
	begin
		delete from [imag].[KolejkaRezerwacji] where IdPozycjiZO=@IdPozycjiZO and IdPozycjiBZD=@IdPozycjiBZD
	end
end
GO
