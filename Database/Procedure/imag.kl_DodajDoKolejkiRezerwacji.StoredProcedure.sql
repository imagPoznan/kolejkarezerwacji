/****** Object:  StoredProcedure [imag].[kl_DodajDoKolejkiRezerwacji]    Script Date: 13.07.2021 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[kl_DodajDoKolejkiRezerwacji]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [imag].[kl_DodajDoKolejkiRezerwacji] AS' 
END
GO
/*
-- =============================================
-- Author:		Karol Łyduch
-- Create date: 2021-07-08
-- Description:	Procedura dodaje pozycje do kolejki rezerwacji
-- =============================================
*/
ALTER	PROCEDURE [imag].[kl_DodajDoKolejkiRezerwacji]
		(@IdPozycjiBZD numeric
		,@IdPozycjiZO numeric
		--,@DoRezerwacji decimal(16,6)
		)
AS
declare	@DoRezerwacji decimal(16,6)
		,@IdKolejki int
BEGIN
	if not exists(select 1 from imag.KolejkaRezerwacji where IdPozycjiZO=@IdPozycjiZO and IdPozycjiBZD=@IdPozycjiBZD)
	begin
		select	@DoRezerwacji=ZAMOWIONO
		from	dbo.POZYCJA_ZAMOWIENIA
		where	ID_POZYCJI_ZAMOWIENIA=@IdPozycjiZO

		set @IdKolejki=(select isnull(max(IdKolejki), 0) from imag.KolejkaRezerwacji where IdPozycjiBZD=@IdPozycjiBZD)+1

		INSERT	INTO [imag].[KolejkaRezerwacji]
				([IdPozycjiBZD]
				,[IdPozycjiZD]
				,[IdPozycjiZO]
				,[DoRezerwacji]
				,[Zarezerwowano]
				,[IdKolejki]
				)
		VALUES	(@IdPozycjiBZD
				,0
				,@IdPozycjiZO
				,@DoRezerwacji
				,0
				,@IdKolejki
				)
	end
END
GO
