/****** Object:  UserDefinedFunction [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]    Script Date: 13.07.2021 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[kl_func_PokazZamowieniaPrzyszlePozycja]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		Karol Łyduch
-- Create date: 2021-07-09
-- Description:	Funkcja zwraca pozycje w zamówieniach przyszłych
-- =============================================
CREATE	FUNCTION [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]
		(@IdPozycji numeric
		)
RETURNS	TABLE 
AS
	RETURN
		select	p.IdPozZamowienia
				,p.Sezon
				,n.IdFabryki
				,n.Fabryka
				,n.DataRealizacji
				--,n.DataRealizacjiSys
				,p.Zamowiono
				,p.Jednostka
				,p.Przelicznik
				,isnull((select 1 from imag.KolejkaRezerwacji where IdPozycjiZO=@IdPozycji and IdPozycjiBZD=p.IdPozZamowienia),0) CzyZarezerwowano
				--,*
		from	imag.ZamowieniaPrzyszlePozycje p
		join	imag.ZamowieniaPrzyszle n on p.Sezon=n.Sezon
		where	IdArtykulu=(select ID_ARTYKULU from dbo.POZYCJA_ZAMOWIENIA where ID_POZYCJI_ZAMOWIENIA=@IdPozycji)
' 
END
GO
