/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzPozycjeZamowienia]    Script Date: 13.07.2021 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[kl_func_PobierzPozycjeZamowienia]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE	FUNCTION [imag].[kl_func_PobierzPozycjeZamowienia]
		(@IdPozZamowienia numeric
		)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	select	a.NAZWA_CALA NazwaArtykulu
			,convert(decimal(16,6), pz.ZAMOWIONO/pz.PRZELICZNIK) Zamowiono
			,pz.JEDNOSTKA Jednostka
			,a.POLE2 NazwaKoloru
			,a.POLE3 NumerKoloru
			,a.INDEKS_KATALOGOWY Indeks
			
	from	dbo.POZYCJA_ZAMOWIENIA pz
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	where	pz.ID_POZYCJI_ZAMOWIENIA=@IdPozZamowienia
)
' 
END
GO
