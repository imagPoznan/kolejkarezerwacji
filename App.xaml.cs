﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KolejkaRezerwacji
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                if (e.Args.Length > 0)
                {
                    WaproExtension.Instance.CheckWaproConnection();
                    WaproExtension.Instance.GetWaproParameters();
                    WaproExtension.Instance.CheckWaproVersion();
                }
                else
                {
                    _ = MessageBox.Show("Nieprawidłowe uruchomienie aplikacji.");
                    Environment.Exit(2);
                }                    
            }
            catch (Exception)
            {
                _ = MessageBox.Show("Nieprawidłowe uruchomienie aplikacji.");
                throw;
            }
        }
    }
}
