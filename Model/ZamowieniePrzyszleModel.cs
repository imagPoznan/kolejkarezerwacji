﻿using KolejkaRezerwacji.ViewModel.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace KolejkaRezerwacji.Model
{
    public class ZamowieniePrzyszleModel : ViewModelBase
    {
        private decimal _zamowiono;
        private bool _czyZarezerwowano;
        public decimal IdPozZamowienia { get; set; }
        public bool CzyZarezerwowano
        {
            get => _czyZarezerwowano;
            set
            {
                if (value == _czyZarezerwowano)
                {
                    _czyZarezerwowano = value;
                    if (value)
                    {
                        WaproExtension.Instance.Zarezerwowane++;
                    }
                }
                else
                {
                    ///Jeżeli pozycję trzeba zarezerwować
                    ///value is true
                    ///_czyZarezerwowano is false
                    if (value && !_czyZarezerwowano)
                    {
                        WaproExtension.Instance.Zarezerwowane++;
                        if (WaproExtension.Instance.Zarezerwowane > 1)
                        {
                            _ = MainWindowViewModel.ShowMessageBox("Można zarezerwować tylko jedną pozycję. Odznacz zaznaczoną pozycję i spróbuj ponownie.");
                            WaproExtension.Instance.Zarezerwowane = 1;
                        }
                        else
                        {
                            WaproExtension.Instance.Zarezerwuj(IdPozZamowienia, WaproExtension.Instance.IdObiektu);
                            _czyZarezerwowano = value;
                        }
                    }
                    ///jeżeli pozycję trzeba odrezerwować
                    ///value is false
                    ///_czyZarezerwowano is true
                    else
                    {
                        WaproExtension.Instance.Zarezerwowane--;
                        WaproExtension.Instance.Odrezerwuj(IdPozZamowienia, WaproExtension.Instance.IdObiektu);
                        _czyZarezerwowano = value;
                    }
                }
                OnPropertyChanged("CzyZarezerwowano");
            }
        }
        public string Sezon { get; set; }
        public decimal IdFabryki { get; set; }
        public string Fabryka { get; set; }
        public DateTime DataRealizacji { get; set; }
        public int DataRealizacjiSys { get; set; }
        public decimal Zamowiono { get => Przelicznik == 1 ? _zamowiono : _zamowiono / Przelicznik; set => _zamowiono = value; }
        public decimal Przelicznik { get; set; }
        public string Jednostka { get; set; }
        public decimal ZamowionoSztuk { get; set; }
    }
}
