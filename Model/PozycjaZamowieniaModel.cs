﻿using KolejkaRezerwacji.ViewModel.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolejkaRezerwacji.Model
{
    public class PozycjaZamowieniaModel : ViewModelBase
    {
        private decimal _zamowiono;
        public string NazwaArtykulu { get; set; }
        public decimal Zamowiono { get => Przelicznik == 1 ? _zamowiono : _zamowiono / Przelicznik; set => _zamowiono = value; }
        public string Jednostka { get; set; }
        public decimal Przelicznik { get; set; }
        public string NazwaKoloru { get; set; }
        public string NumerKoloru { get; set; }
        public string Indeks { get; set; }
    }
}
